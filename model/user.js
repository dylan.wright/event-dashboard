const  Sequelize = require('sequelize');
import { getDatabaseClient } from '../config/database';

export const userTable = async () => {
const sequelize = await getDatabaseClient();

return await sequelize.define("user", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING
    },
    city: {
      type: Sequelize.STRING
    }
  });
};