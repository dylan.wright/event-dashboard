import dotenv from 'dotenv';

dotenv.config();

export function getRequestWithAuthHeader(path) {
    return {
        "method": "GET",
        "hostname": process.env.TICKET_TAILOR_HOST,
        "port": null,
        "path": path.toString(),
        "headers": {
          "Accept": "application/json",
          "Content-Length": "0",
          "Authorization": getTicketTailorAuth()
        }
      };
}


export function getTicketTailorAuth() {
  var apiKey = process.env.TICKET_TAILOR_API_KEY;
  return "Basic " + Buffer.from(apiKey).toString("base64");
}
