import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { corsOptions } from './middleware/cors.js';
import api from './routes/routes.js';

const port = process.env.PORT || 8888;
const app = express();
app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
})); 


app.use('/api', api);
app.use(cors(corsOptions))
app.listen(port, () => console.log(`Listening on port ${port}`));
