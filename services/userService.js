import { findAllUsers, addUserToDb } from "../repository/userRepository"

export const getUsers = async () => {
  const users = await findAllUsers();
  return users;
  
}

export const addUserObj = async (userObj) => {
  console.log('service ', userObj);
  const users = await addUserToDb(userObj);
  return users;
}