import axios from 'axios';
import { getTicketTailorAuth } from '../utils/restUtils'


export const getIssuedTickets = async () => {

const issuedTicketsUrl = '/v1/issued_tickets';

const response = await axios.get(process.env.TICKET_TAILOR_HOST + issuedTicketsUrl, 
{
  headers: {
    "Accept": "application/json",
    "Authorization": getTicketTailorAuth()
    }
  })
  
  return response.data;
  
}