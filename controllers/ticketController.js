import { getIssuedTickets } from '../services/ticketService.js';

export const validateTicket = async (req, res) => {

  console.log("Validating Ticket");

  const { email, barcode } = req.query;

  const tickets = await getIssuedTickets();

  let isValidTicket = validateBarcode(tickets["data"], barcode, email);

  isValidTicket ? console.log(`Ticket '${barcode}' is valid!`) : console.log(`Ticket '${barcode}' is invalid!`)

  res.send({ isValidTicket: isValidTicket});
  };


function validateBarcode(tickets, barcode, email) {
  // Check username and barcode match an issued ticket
  const validTicket = tickets.find(order => order["email"] === email && order["barcode"] === barcode);

  return validTicket ? true : false
}
