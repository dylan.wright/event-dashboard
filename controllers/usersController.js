import { getUsers, addUserObj } from '../services/userService';

export const getAllUsers = async (req, res) => {

  const users = await getUsers();
  res.send({ users: users});
  };


export const addUser = async (req, res) => {

    console.log('controller body ', req.body);

  const users = await addUserObj(req.body);
  res.send(users);
  };


