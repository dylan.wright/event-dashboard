import express from 'express';
import { validateTicket } from '../controllers/ticketController.js';
import { addUser, getAllUsers } from '../controllers/usersController.js';
const router = express.Router();

router.get('/validate', validateTicket);
router.get('/user', getAllUsers);
router.post('/user', addUser);

export default router;
