README - event-dashboard



Config

`.env` file is populated with default values:

`TICKET_TAILOR_API_KEY=sk_2734_122713_2b4dc969a9a1544ee0777c2c08a26f7f:`    - Api key for Ticket Tailor authentication 
`TICKET_TAILOR_HOST=api.tickettailor.com`   - Ticket Tailor base url
`CLIENT_URL=localhost:3000`.  - Client Url added to cors whitelist


Run Instructions

Navigate to `dashboard-poc/app`:

`npm run start` - will start the node server on `localhost:8888`


Request Mappings

Local Development:

`http://localhost:8888/api/validate?email={emailValue}%40and.digital&barcode={barcodeValue}` - returns true or false if barcode and email matches tickets issued.
