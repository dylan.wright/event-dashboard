import { userTable } from '../model/user.js';

export const findAllUsers = async () => {

    const user = await userTable();
    await user.sync();
    const users = await user.findAll();

    return users;

}

export const addUserToDb = async (userObj) => {
    console.log("Adding user to the database: ", userObj.name)
    const user = await userTable();
    await user.sync();
    return await user.create({ id: (Math.random() * 100), name: userObj.name, city: userObj.city});
}