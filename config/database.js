import { Sequelize } from 'sequelize';

export const getDatabaseClient = async () => {

    return new Sequelize({
        dialect: 'mariadb',
        dialectOptions: {
        host: process.env.DATABASE_HOST, 
        database: process.env.DATABASE,
        user: process.env.DATABASE_USER, 
        password: process.env.DATABASE_PASSWORD,
        port: process.env.PORT
    }
});
}
